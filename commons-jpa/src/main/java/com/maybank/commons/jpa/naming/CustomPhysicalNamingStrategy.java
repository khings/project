package com.maybank.commons.jpa.naming;

import com.maybank.commons.jpa.naming.util.NamingUtil;
import java.io.Serializable;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

public class CustomPhysicalNamingStrategy extends PhysicalNamingStrategyStandardImpl
    implements Serializable {
  private static final long serialVersionUID = 5384199549904872700L;

  @Override
  public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
    return new Identifier(NamingUtil.addUnderscores(name.getText()), name.isQuoted());
  }

  @Override
  public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment context) {
    return new Identifier(NamingUtil.addUnderscores(name.getText()), name.isQuoted());
  }
}
