package com.maybank.commons.jpa.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/** Configuration to enable auditing in JPA via annotation configuration. */
@Configuration
@EnableJpaAuditing
public class AuditingConfig {}
