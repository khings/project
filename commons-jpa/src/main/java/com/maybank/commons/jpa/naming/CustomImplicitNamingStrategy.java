package com.maybank.commons.jpa.naming;

import com.maybank.commons.jpa.naming.util.NamingUtil;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitJoinColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitJoinTableNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;

public class CustomImplicitNamingStrategy extends ImplicitNamingStrategyJpaCompliantImpl {
  private static final long serialVersionUID = -6890507181694096836L;

  public CustomImplicitNamingStrategy() {}

  @Override
  public Identifier determineJoinTableName(ImplicitJoinTableNameSource source) {
    String nonOwningName = null;
    if (source.getAssociationOwningAttributePath() == null) {
      if (source.getNonOwningEntityNaming().getJpaEntityName() == null) {
        nonOwningName = source.getNonOwningPhysicalTableName();
      } else {
        nonOwningName = source.getNonOwningEntityNaming().getJpaEntityName();
      }
    } else {
      nonOwningName = source.getAssociationOwningAttributePath().getProperty();
    }
    final String name =
        NamingUtil.addUnderscores(source.getOwningPhysicalTableName() + '_' + nonOwningName);
    return toIdentifier(name, source.getBuildingContext());
  }

  @Override
  public Identifier determineJoinColumnName(ImplicitJoinColumnNameSource source) {
    final String name;

    if (source.getNature() == ImplicitJoinColumnNameSource.Nature.ELEMENT_COLLECTION
        || source.getAttributePath() == null) {
      name = NamingUtil.addUnderscores(transformEntityName(source.getEntityNaming()));
    } else {
      name = NamingUtil.addUnderscores(source.getAttributePath().getProperty());
    }
    return toIdentifier(name, source.getBuildingContext());
  }
}
