package com.maybank.commons.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class HttpUtil {
  private static final Logger LOG = LoggerFactory.getLogger(HttpUtil.class);

  private HttpUtil() {}

  public static String get(
      String url, List<NameValuePair> getParams, int connectionTimeout, int readTimeout)
      throws Exception {
    String result = null;

    try {
      LOG.debug(
          "[SENDING] url:[{}], getParams:[{}], connectionTimeout:[{}ms], readTimeout:[{}ms]...",
          url,
          getParams,
          connectionTimeout,
          readTimeout);

      RequestConfig requestConfig =
          RequestConfig.custom()
              .setSocketTimeout(readTimeout)
              .setConnectTimeout(connectionTimeout)
              .build();

      try (CloseableHttpClient client =
          HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build()) {

        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Connection", "close");
        if (getParams != null && getParams.size() > 0) {
          URI uri = new URIBuilder(httpGet.getURI()).addParameters(getParams).build();
          httpGet.setURI(uri);
        }

        try (CloseableHttpResponse response = client.execute(httpGet)) {
          int responseCode = response.getStatusLine().getStatusCode();
          LOG.debug("GET responseCode:[{}]", responseCode);

          if (responseCode == HttpStatus.SC_OK) {
            HttpEntity httpEntity = response.getEntity();
            if (httpEntity != null) {
              InputStream is = httpEntity.getContent();
              result = convertInputStreamToString(is);
              if (result == null || result.trim().length() == 0) {
                result = String.valueOf(responseCode);
              }
            } else {
              result = String.valueOf(responseCode);
            }
            LOG.debug("GET result:[{}]", result);
          } else {
            result = response.getStatusLine().getReasonPhrase();
            if (result == null || result.trim().length() == 0) {
              result = String.valueOf(responseCode);
            } else {
              result = responseCode + ", " + result;
            }
            LOG.debug("GET result:[{}]", result);

            throw new HttpException(result);
          }
        }
      }

    } catch (Exception e) {
      throw e;
    }

    return result;
  }

  private static String convertInputStreamToString(InputStream is) throws IOException {
    if (is != null) {
      StringBuilder sb = new StringBuilder();
      String line;
      boolean isFirst = true;
      try {
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        while ((line = br.readLine()) != null) {
          if (isFirst) {
            isFirst = false;
            sb.append(line);
          } else {
            sb.append("\n").append(line);
          }
        }
      } finally {
        is.close();
      }
      return sb.toString();
    }
    return "";
  }
}
