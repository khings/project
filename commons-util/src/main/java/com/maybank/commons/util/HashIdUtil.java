package com.maybank.commons.util;

import org.hashids.Hashids;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class HashIdUtil {
  private static final Logger LOG = LoggerFactory.getLogger(HashIdUtil.class);
  private static final String HASHIDS_ALPHABET = "abcdefghijkmnpqrstuvwxyz23456789";

  private HashIdUtil() {}

  public static String encodeHashIds(String salt, long... data) {
    Hashids hashids = new Hashids(salt, 0, HASHIDS_ALPHABET);
    return hashids.encode(data);
  }

  public static long[] decodeHashIds(String salt, String hash) {
    Hashids hashids = new Hashids(salt, 0, HASHIDS_ALPHABET);
    try {
      long[] ids = hashids.decode(hash);
      if (ids.length > 0) {
        return ids;
      } else {
        LOG.error("Failed to decode hash id by salt:[{}], hash:[{}]", salt, hash);
        return null;
      }
    } catch (Exception e) {
      LOG.error(String.format("Failed to decode hash id by salt:[%s], hash:[%s]", salt, hash), e);
      return null;
    }
  }
}
