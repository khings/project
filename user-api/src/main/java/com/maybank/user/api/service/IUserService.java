package com.maybank.user.api.service;

import com.maybank.user.api.dto.AppUserDTO;
import com.maybank.user.api.dto.AppUserListDTO;
import com.maybank.user.api.dto.UpdateAppUserRequestDTO;
import com.maybank.user.api.service.exception.ServiceException;

public interface IUserService {
  String registerAppUser(AppUserDTO appUserDTO) throws ServiceException;

  void updateAppUser(UpdateAppUserRequestDTO updateAppUserRequestDTO) throws ServiceException;

  AppUserListDTO listAppUsers(Integer pageNumber, Integer pageSize) throws ServiceException;

  AppUserDTO getAppUserDetail(String hashUserId) throws ServiceException;
}
