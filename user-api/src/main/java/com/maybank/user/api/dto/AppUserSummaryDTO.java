package com.maybank.user.api.dto;

import com.maybank.commons.util.HashIdUtil;
import com.maybank.user.api.constants.UserConstants;

public class AppUserSummaryDTO {
  private String userHashId;
  private String firstName;
  private String lastName;
  private String email;

  public AppUserSummaryDTO() {}

  public AppUserSummaryDTO(long id, String firstName, String lastName, String email) {
    this.userHashId =
        HashIdUtil.encodeHashIds(UserConstants.HASH_ID_SALT, id, System.currentTimeMillis());
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }

  public String getUserHashId() {
    return userHashId;
  }

  public void setUserHashId(String userHashId) {
    this.userHashId = userHashId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AppUserSummaryDTO [userHashId=");
    builder.append(userHashId);
    builder.append(", firstName=");
    builder.append(firstName);
    builder.append(", lastName=");
    builder.append(lastName);
    builder.append(", email=");
    builder.append(email);
    builder.append("]");
    return builder.toString();
  }
}
