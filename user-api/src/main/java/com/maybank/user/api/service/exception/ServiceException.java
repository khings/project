package com.maybank.user.api.service.exception;

import com.google.common.base.Joiner;
import com.maybank.user.api.constants.Error;

public class ServiceException extends RuntimeException {
  private static final long serialVersionUID = -4711100055313140321L;
  private static final Joiner JOINER = Joiner.on(". ").skipNulls();
  private String errorCode;

  public ServiceException(String errorCode, String message) {
    super(message);
    this.errorCode = errorCode;
  }

  public ServiceException(String errorCode, String message, Exception e) {
    super(message, e);
    this.errorCode = errorCode;
  }

  public ServiceException(Error error, String... additionalMessages) {
    super(
        error.getMsg()
            + (additionalMessages.length == 0
                ? ""
                : ". " + JOINER.join(additionalMessages).trim()));
    this.errorCode = error.getCode();
  }

  public ServiceException(Error error, Exception e) {
    super(String.format("%s. %s", error.getMsg(), e.getMessage()), e);
    this.errorCode = error.getCode();
  }

  public String getErrorCode() {
    return this.errorCode;
  }
}
