package com.maybank.user.api.entity;

import com.maybank.commons.jpa.entity.BaseEntity;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(
    name = "app_user",
    indexes = {@Index(name = "idx_email", columnList = "email", unique = true)})
public class AppUserEntity extends BaseEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @NotBlank
  @Size(max = 100)
  @Column(nullable = false, length = 100)
  private String firstName;

  @NotBlank
  @Size(max = 100)
  @Column(nullable = false, length = 100)
  private String lastName;

  // Email address must be unique across entire app_user table.
  @Email
  @NotBlank
  @Size(max = 320)
  @Column(nullable = false, length = 320)
  private String email;

  @NotNull
  @Valid
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "address_id", nullable = false)
  protected AddressEntity address;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public AddressEntity getAddress() {
    return address;
  }

  public void setAddress(AddressEntity address) {
    this.address = address;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AppUserEntity [id=");
    builder.append(id);
    builder.append(", firstName=");
    builder.append(firstName);
    builder.append(", lastName=");
    builder.append(lastName);
    builder.append(", email=");
    builder.append(email);
    builder.append(", address=");
    builder.append(address);
    builder.append(", version=");
    builder.append(getVersion());
    builder.append(", createdAt=");
    builder.append(getCreatedAt());
    builder.append(", updatedAt=");
    builder.append(getUpdatedAt());
    builder.append("]");
    return builder.toString();
  }
}
