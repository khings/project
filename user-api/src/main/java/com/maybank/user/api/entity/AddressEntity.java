package com.maybank.user.api.entity;

import com.maybank.commons.jpa.entity.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "address")
public class AddressEntity extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @NotBlank
  @Size(max = 500)
  @Column(nullable = false, length = 500)
  private String addressLine1;

  @NotBlank
  @Size(max = 50)
  @Column(nullable = false, length = 50)
  private String city;

  @NotBlank
  @Size(max = 50)
  @Column(nullable = false, length = 50)
  private String state;

  @NotBlank
  @Size(max = 5)
  @Column(nullable = false, length = 5)
  private String postcode;

  @NotBlank
  @Size(max = 2)
  @Column(nullable = false, length = 2, columnDefinition = "char(2)")
  private String countryCode;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AddressEntity [id=");
    builder.append(id);
    builder.append(", addressLine1=");
    builder.append(addressLine1);
    builder.append(", city=");
    builder.append(city);
    builder.append(", state=");
    builder.append(state);
    builder.append(", postcode=");
    builder.append(postcode);
    builder.append(", countryCode=");
    builder.append(countryCode);
    builder.append("]");
    return builder.toString();
  }
}
