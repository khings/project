package com.maybank.user.api.service.impl;

import com.maybank.commons.util.HashIdUtil;
import com.maybank.user.api.constants.Error;
import com.maybank.user.api.constants.UserConstants;
import com.maybank.user.api.dto.AddressDTO;
import com.maybank.user.api.dto.AppUserDTO;
import com.maybank.user.api.dto.AppUserListDTO;
import com.maybank.user.api.dto.AppUserSummaryDTO;
import com.maybank.user.api.dto.UpdateAppUserRequestDTO;
import com.maybank.user.api.entity.AddressEntity;
import com.maybank.user.api.entity.AppUserEntity;
import com.maybank.user.api.persistence.IAppUserRepository;
import com.maybank.user.api.service.IAddressService;
import com.maybank.user.api.service.IUserService;
import com.maybank.user.api.service.exception.ServiceException;
import com.maybank.user.api.validator.RequestValidator;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements IUserService {
  @Autowired private IAddressService addressService;
  @Autowired private IAppUserRepository appUserRepo;
  @Autowired private RequestValidator requestValidator;

  @Transactional
  @Override
  public String registerAppUser(AppUserDTO appUserDTO) throws ServiceException {
    try {
      validateRegisterAppUserRequest(appUserDTO);
      AppUserEntity appUserEntity = new AppUserEntity();
      BeanUtils.copyProperties(appUserDTO, appUserEntity);
      AddressEntity addressEntity = new AddressEntity();
      BeanUtils.copyProperties(appUserDTO.getAddress(), addressEntity);
      appUserEntity.setAddress(addressEntity);
      appUserRepo.save(appUserEntity);
      return HashIdUtil.encodeHashIds(
          UserConstants.HASH_ID_SALT, appUserEntity.getId(), System.currentTimeMillis());
    } catch (ServiceException e) {
      throw e;
    } catch (Exception e) {
      throw new ServiceException(Error.REG_APP_USER_SVC_EXCEPTION, e);
    }
  }

  @Transactional
  @Override
  public void updateAppUser(UpdateAppUserRequestDTO updateAppUserRequestDTO)
      throws ServiceException {
    try {
      requestValidator.validate(updateAppUserRequestDTO);

      long[] decodedIds =
          HashIdUtil.decodeHashIds(
              UserConstants.HASH_ID_SALT, updateAppUserRequestDTO.getUserHashId());
      if (decodedIds == null) {
        throw new ServiceException(Error.INVALID_USER_HASH_ID);
      }
      long userId = decodedIds[0];

      AppUserEntity appUserEntity =
          appUserRepo
              .findById(userId)
              .orElseThrow(() -> new ServiceException(Error.USER_NOT_FOUND));
      AppUserDTO updatedAppUserDTO = updateAppUserRequestDTO.getAppUser();
      validateUpdateAppUserRequest(updatedAppUserDTO, appUserEntity);
      BeanUtils.copyProperties(updatedAppUserDTO, appUserEntity);
      AddressEntity addressEntity = appUserEntity.getAddress();
      BeanUtils.copyProperties(updatedAppUserDTO.getAddress(), addressEntity);
      appUserEntity.setAddress(addressEntity);
      appUserRepo.save(appUserEntity);
    } catch (ServiceException e) {
      throw e;
    } catch (Exception e) {
      throw new ServiceException(Error.UPDATE_APP_USER_SVC_EXCEPTION, e);
    }
  }

  @Override
  public AppUserListDTO listAppUsers(Integer pageNumber, Integer pageSize) throws ServiceException {
    try {
      if (pageNumber == null || pageNumber < 1) {
        pageNumber = UserConstants.DEFAULT_PAGE_NUM;
      }
      if (pageSize == null || pageSize < 1) {
        pageSize = UserConstants.DEFAULT_PAGE_SIZE;
      }

      Page<AppUserSummaryDTO> page =
          appUserRepo.getUsers(
              PageRequest.of(pageNumber - 1, pageSize, Sort.by(Direction.ASC, "createdAt")));
      if (page.hasContent()) {
        AppUserListDTO listDTO = new AppUserListDTO();
        listDTO.setAppUsers(page.getContent());
        listDTO.setHasNext(page.hasNext());
        return listDTO;
      }
      return new AppUserListDTO();
    } catch (ServiceException e) {
      throw e;
    } catch (Exception e) {
      throw new ServiceException(Error.LIST_APP_USERS_SVC_EXCEPTION, e);
    }
  }

  @Override
  public AppUserDTO getAppUserDetail(String userHashId) throws ServiceException {
    try {
      long[] decodedIds = HashIdUtil.decodeHashIds(UserConstants.HASH_ID_SALT, userHashId);
      if (decodedIds == null) {
        throw new ServiceException(Error.INVALID_USER_HASH_ID);
      }
      long userId = decodedIds[0];

      AppUserEntity appUserEntity =
          appUserRepo
              .findById(userId)
              .orElseThrow(() -> new ServiceException(Error.USER_NOT_FOUND));
      AppUserDTO appUserDTO = new AppUserDTO();
      BeanUtils.copyProperties(appUserEntity, appUserDTO);
      AddressDTO addressDTO = new AddressDTO();
      BeanUtils.copyProperties(appUserEntity.getAddress(), addressDTO);
      appUserDTO.setAddress(addressDTO);
      return appUserDTO;
    } catch (ServiceException e) {
      throw e;
    } catch (Exception e) {
      throw new ServiceException(Error.GET_APP_USER_DETAIL_SVC_EXCEPTION, e);
    }
  }

  private void validateRegisterAppUserRequest(AppUserDTO appUserDTO) {
    requestValidator.validate(appUserDTO);

    if (appUserRepo.countByEmail(appUserDTO.getEmail()) > 0) {
      throw new ServiceException(Error.DUPLICATED_EMAIL);
    }

    AddressDTO addressDTO = appUserDTO.getAddress();
    addressService.verifyPostcode(addressDTO.getPostcode(), addressDTO.getCountryCode());
  }

  private void validateUpdateAppUserRequest(
      AppUserDTO updatedAppUserDTO, AppUserEntity appUserEntity) {
    String updatedEmail = updatedAppUserDTO.getEmail();
    if (!appUserEntity.getEmail().equals(updatedEmail)
        && appUserRepo.countByEmail(updatedEmail) > 0) {
      throw new ServiceException(Error.DUPLICATED_EMAIL);
    }

    AddressDTO updateAddressDTO = updatedAppUserDTO.getAddress();
    if (!appUserEntity.getAddress().getPostcode().equals(updateAddressDTO.getPostcode())) {
      addressService.verifyPostcode(
          updateAddressDTO.getPostcode(), updateAddressDTO.getCountryCode());
    }
  }
}
