package com.maybank.user.api.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdateAppUserRequestDTO {
  @NotBlank private String userHashId;
  @NotNull @Valid private AppUserDTO appUser;

  public String getUserHashId() {
    return userHashId;
  }

  public void setUserHashId(String userHashId) {
    this.userHashId = userHashId;
  }

  public AppUserDTO getAppUser() {
    return appUser;
  }

  public void setAppUser(AppUserDTO appUser) {
    this.appUser = appUser;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("UpdateAppUserRequestDTO [userHashId=");
    builder.append(userHashId);
    builder.append(", appUser=");
    builder.append(appUser);
    builder.append("]");
    return builder.toString();
  }
}
