package com.maybank.user.api.dto;

public class GenericResponseDTO<T> extends BaseResponseDTO {
  private T responseData;

  public GenericResponseDTO() {}

  public GenericResponseDTO(String responseCode, String responseMessage) {
    super(responseCode, responseMessage);
  }

  public GenericResponseDTO(String responseCode, String responseMessage, T responseData) {
    super(responseCode, responseMessage);
    this.responseData = responseData;
  }

  public T getResponseData() {
    return responseData;
  }

  public void setResponseData(T responseData) {
    this.responseData = responseData;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("GenericResponseDTO [responseData=");
    builder.append(responseData);
    builder.append(", responseCode=");
    builder.append(getResponseCode());
    builder.append(", responseMessage=");
    builder.append(getResponseMessage());
    builder.append("]");
    return builder.toString();
  }
}
