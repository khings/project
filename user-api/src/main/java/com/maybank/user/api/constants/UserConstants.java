package com.maybank.user.api.constants;

public class UserConstants {
  public static final String HASH_ID_SALT = "194272c1-06a5-418a-b393-17201b1415ac";
  public static final int DEFAULT_PAGE_NUM = 1;
  public static final int DEFAULT_PAGE_SIZE = 10;
}
