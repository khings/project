package com.maybank.user.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.maybank.commons.jpa", "com.maybank.user.api"})
@EnableJpaRepositories(basePackages = {"com.maybank.user.api"})
@EntityScan(basePackages = {"com.maybank.user.api"})
public class ApiApplication extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(ApiApplication.class, args);
  }
}
