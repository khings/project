package com.maybank.user.api.service;

import com.maybank.user.api.service.exception.ServiceException;

public interface IAddressService {
  void verifyPostcode(String postcode, String countryCode) throws ServiceException;
}
