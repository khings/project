package com.maybank.user.api.validator;

import com.maybank.user.api.constants.Error;
import com.maybank.user.api.service.exception.ServiceException;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RequestValidator {
  @Autowired private Validator validator;

  public <E> void validate(E request) {
    Set<ConstraintViolation<Object>> violations = validator.validate(request);
    if (violations.size() > 0) {
      throw new ServiceException(Error.ILLEGAL_ARGS, violations.toString());
    }
  }
}
