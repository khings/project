package com.maybank.user.api.persistence;

import com.maybank.user.api.dto.AppUserSummaryDTO;
import com.maybank.user.api.entity.AppUserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface IAppUserRepository extends CrudRepository<AppUserEntity, Long> {
  int countByEmail(String email);

  @Query(
      "select new com.maybank.user.api.dto.AppUserSummaryDTO(u.id, u.firstName, u.lastName, u.email) from AppUserEntity u")
  Page<AppUserSummaryDTO> getUsers(Pageable pageable);
}
