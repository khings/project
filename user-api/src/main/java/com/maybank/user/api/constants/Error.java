package com.maybank.user.api.constants;

public enum Error {
  // Success code
  SUCCESS("0", "Success"),

  // Common error
  ILLEGAL_ARGS("1", "Illegal arguments"),
  DUPLICATED_EMAIL("2", "The same email address has been used by another user"),
  INVALID_POSTCODE("3", "Invalid postcode"),
  VERIFY_POSTCODE_FAILURE("4", "Failed to verify postcode"),
  USER_NOT_FOUND("5", "User does not exists"),
  INVALID_USER_HASH_ID("6", "Invalid user hash id"),

  // Controller error
  REG_APP_USER_CTL_EXCEPTION("1001", "Exception occurs in UserController -> registerAppUser"),
  UPDATE_APP_USER_CTL_EXCEPTION("1002", "Exception occurs in UserController -> updateAppUser"),
  LIST_APP_USERS_CTL_EXCEPTION("1003", "Exception occurs in UserController -> listAppUsers"),
  GET_APP_USER_DETAIL_CTL_EXCEPTION(
      "1004", "Exception occurs in UserController -> getAppUserDetail"),

  // Service error
  REG_APP_USER_SVC_EXCEPTION("2001", "Exception occurs in UserServiceImpl -> registerAppUser"),
  UPDATE_APP_USER_SVC_EXCEPTION("2002", "Exception occurs in UserServiceImpl -> updateAppUser"),
  LIST_APP_USERS_SVC_EXCEPTION("2003", "Exception occurs in UserServiceImpl -> listAppUsers"),
  GET_APP_USER_DETAIL_SVC_EXCEPTION(
      "2004", "Exception occurs in UserServiceImpl -> getAppUserDetail"),
  VERIFY_POSTCODE_SVC_EXCEPTION("2005", "Exception occurs in AddressServiceImpl -> verifyPostcode");

  private String code;
  private String msg;

  private Error(String code, String msg) {
    this.code = code;
    this.msg = msg;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }
}
