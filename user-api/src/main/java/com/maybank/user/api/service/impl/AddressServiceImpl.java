package com.maybank.user.api.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.maybank.commons.util.HttpUtil;
import com.maybank.user.api.constants.Error;
import com.maybank.user.api.service.IAddressService;
import com.maybank.user.api.service.exception.ServiceException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpException;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements IAddressService {
  @Value("${zipcodebase.api-key}")
  private String zipcodebaseApiKey;

  @Value("${zipcodebase.search-url}")
  private String zipcodebaseSearchUrl;

  @Value("${zipcodebase.connection-timeout:30000}")
  private int zipcodebaseConnTimeout;

  @Value("${zipcodebase.read-timeout:60000}")
  private int zipcodebaseReadTimeout;

  private static final String PARAM_KEY_API_KEY = "apikey";
  private static final String PARAM_KEY_CODES = "codes";
  private static final String PARAM_KEY_COUNTRY = "country";
  private static final String MAP_KEY_RESULTS = "results";
  private final ObjectMapper mapper =
      new ObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
  private final TypeReference<Map<String, Object>> type = new TypeReference<>() {};

  @Override
  public void verifyPostcode(String postcode, String countryCode) throws ServiceException {
    try {
      List<NameValuePair> getParams = new ArrayList<>();
      getParams.add(new BasicNameValuePair(PARAM_KEY_API_KEY, zipcodebaseApiKey));
      getParams.add(new BasicNameValuePair(PARAM_KEY_CODES, postcode));
      getParams.add(new BasicNameValuePair(PARAM_KEY_COUNTRY, countryCode));
      String responseJson =
          HttpUtil.get(
              zipcodebaseSearchUrl, getParams, zipcodebaseConnTimeout, zipcodebaseReadTimeout);
      Map<String, Object> responseMap = mapper.readValue(responseJson, type);
      Object resultsObject = responseMap.get(MAP_KEY_RESULTS);
      if (resultsObject instanceof Map) {
        Map<String, Object> resultsMap = (Map<String, Object>) resultsObject;
        // Postcode matched
        if (resultsMap.get(postcode) != null) return;
      }
      throw new ServiceException(Error.INVALID_POSTCODE);
    } catch (ServiceException e) {
      throw e;
    } catch (HttpException e) {
      throw new ServiceException(Error.VERIFY_POSTCODE_FAILURE, e);
    } catch (Exception e) {
      throw new ServiceException(Error.VERIFY_POSTCODE_SVC_EXCEPTION, e);
    }
  }
}
