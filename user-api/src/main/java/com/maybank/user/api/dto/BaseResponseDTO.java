package com.maybank.user.api.dto;

public class BaseResponseDTO {
  private String responseCode;
  private String responseMessage;

  public BaseResponseDTO() {}

  public BaseResponseDTO(String responseCode, String responseMessage) {
    this.responseCode = responseCode;
    this.responseMessage = responseMessage;
  }

  public String getResponseCode() {
    return responseCode;
  }

  public void setResponseCode(String responseCode) {
    this.responseCode = responseCode;
  }

  public String getResponseMessage() {
    return responseMessage;
  }

  public void setResponseMessage(String responseMessage) {
    this.responseMessage = responseMessage;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("BaseResponseDTO [responseCode=");
    builder.append(responseCode);
    builder.append(", responseMessage=");
    builder.append(responseMessage);
    builder.append("]");
    return builder.toString();
  }
}
