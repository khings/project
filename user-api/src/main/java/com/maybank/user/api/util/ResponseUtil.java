package com.maybank.user.api.util;

import com.google.common.base.Joiner;
import com.maybank.user.api.constants.Error;
import com.maybank.user.api.dto.BaseResponseDTO;
import com.maybank.user.api.dto.GenericResponseDTO;
import com.maybank.user.api.service.exception.ServiceException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ResponseUtil {
  private static final Joiner JOINER = Joiner.on(". ").skipNulls();

  private ResponseUtil() {}

  public static BaseResponseDTO constructBaseResponse(ServiceException e) {
    return new BaseResponseDTO(e.getErrorCode(), e.getMessage());
  }

  public static BaseResponseDTO constructBaseResponse(Error error, String... additionalMessages) {
    List<String> errorMessages = new ArrayList<>();
    errorMessages.add(error.getMsg());
    Collections.addAll(errorMessages, additionalMessages);
    return new BaseResponseDTO(error.getCode(), JOINER.join(errorMessages).trim());
  }

  public static <T> GenericResponseDTO<T> constructGenericResponse(ServiceException e) {
    return new GenericResponseDTO<T>(e.getErrorCode(), e.getMessage());
  }

  public static <T> GenericResponseDTO<T> constructGenericResponse(
      Error error, String... additionalMessages) {
    List<String> errorMessages = new ArrayList<>();
    errorMessages.add(error.getMsg());
    Collections.addAll(errorMessages, additionalMessages);
    return new GenericResponseDTO<T>(error.getCode(), JOINER.join(errorMessages).trim());
  }
}
