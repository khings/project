package com.maybank.user.api.dto;

import java.util.ArrayList;
import java.util.List;

public class AppUserListDTO {
  private List<AppUserSummaryDTO> appUsers = new ArrayList<>();
  private boolean hasNext;

  public List<AppUserSummaryDTO> getAppUsers() {
    return appUsers;
  }

  public void setAppUsers(List<AppUserSummaryDTO> appUsers) {
    this.appUsers = appUsers;
  }

  public boolean isHasNext() {
    return hasNext;
  }

  public void setHasNext(boolean hasNext) {
    this.hasNext = hasNext;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AppUserListDTO [appUsers=");
    builder.append(appUsers);
    builder.append(", hasNext=");
    builder.append(hasNext);
    builder.append("]");
    return builder.toString();
  }
}
