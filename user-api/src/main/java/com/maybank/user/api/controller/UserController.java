package com.maybank.user.api.controller;

import com.maybank.user.api.constants.Error;
import com.maybank.user.api.dto.AppUserDTO;
import com.maybank.user.api.dto.AppUserListDTO;
import com.maybank.user.api.dto.BaseResponseDTO;
import com.maybank.user.api.dto.GenericResponseDTO;
import com.maybank.user.api.dto.UpdateAppUserRequestDTO;
import com.maybank.user.api.service.IUserService;
import com.maybank.user.api.service.exception.ServiceException;
import com.maybank.user.api.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"/user"})
public class UserController {
  private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
  @Autowired private IUserService userService;

  @RequestMapping(
      value = "/register",
      method = {RequestMethod.POST})
  public GenericResponseDTO<String> registerAppUser(@RequestBody AppUserDTO appUserDTO) {
    try {
      LOG.debug("[REQ] registerAppUser, request:[{}]", appUserDTO);
      String userHashId = userService.registerAppUser(appUserDTO);
      GenericResponseDTO<String> responseDTO =
          new GenericResponseDTO<>(Error.SUCCESS.getCode(), Error.SUCCESS.getMsg(), userHashId);
      LOG.debug("[RES] registerAppUser, response:[{}]", responseDTO);
      return responseDTO;
    } catch (ServiceException e) {
      LOG.error("Failed to register app user", e);
      return ResponseUtil.constructGenericResponse(e);
    } catch (Exception e) {
      LOG.error("Failed to register app user", e);
      return ResponseUtil.constructGenericResponse(
          Error.REG_APP_USER_CTL_EXCEPTION, e.getMessage());
    }
  }

  @RequestMapping(
      value = "/update",
      method = {RequestMethod.POST}) // RequestMethod can be changed to PUT if required.
  public BaseResponseDTO updateAppUser(
      @RequestBody UpdateAppUserRequestDTO updateAppUserRequestDTO) {
    try {
      LOG.debug("[REQ] updateAppUser, request:[{}]", updateAppUserRequestDTO);
      userService.updateAppUser(updateAppUserRequestDTO);
      BaseResponseDTO responseDTO =
          new BaseResponseDTO(Error.SUCCESS.getCode(), Error.SUCCESS.getMsg());
      LOG.debug("[RES] updateAppUser, response:[{}]", responseDTO);
      return responseDTO;
    } catch (ServiceException e) {
      LOG.error("Failed to update app user", e);
      return ResponseUtil.constructBaseResponse(e);
    } catch (Exception e) {
      LOG.error("Failed to update app user", e);
      return ResponseUtil.constructBaseResponse(
          Error.UPDATE_APP_USER_CTL_EXCEPTION, e.getMessage());
    }
  }

  @RequestMapping(
      value = {"/list", "/list/{pageNumber}", "/list/{pageNumber}/{pageSize}"},
      method = {RequestMethod.GET})
  public GenericResponseDTO<AppUserListDTO> listAppUsers(
      @PathVariable(value = "pageNumber", required = false) Integer pageNumber,
      @PathVariable(value = "pageSize", required = false) Integer pageSize) {
    try {
      LOG.debug("[REQ] listAppUsers, request:[pageNumber:{}, pageSize:{}]", pageNumber, pageSize);
      AppUserListDTO appUserListDTO = userService.listAppUsers(pageNumber, pageSize);
      GenericResponseDTO<AppUserListDTO> responseDTO =
          new GenericResponseDTO<>(Error.SUCCESS.getCode(), Error.SUCCESS.getMsg(), appUserListDTO);
      LOG.debug("[RES] listAppUsers, response:[{}]", responseDTO);
      return responseDTO;
    } catch (ServiceException e) {
      LOG.error("Failed to list app users", e);
      return ResponseUtil.constructGenericResponse(e);
    } catch (Exception e) {
      LOG.error("Failed to list app users", e);
      return ResponseUtil.constructGenericResponse(
          Error.LIST_APP_USERS_CTL_EXCEPTION, e.getMessage());
    }
  }

  @RequestMapping(
      value = "/detail/{userHashId}",
      method = {RequestMethod.GET})
  public GenericResponseDTO<AppUserDTO> getAppUserDetail(
      @PathVariable(value = "userHashId") String userHashId) {
    try {
      LOG.debug("[REQ] getAppUserDetail, request:[userHashId:{}]", userHashId);
      AppUserDTO appUserDTO = userService.getAppUserDetail(userHashId);
      GenericResponseDTO<AppUserDTO> responseDTO =
          new GenericResponseDTO<>(Error.SUCCESS.getCode(), Error.SUCCESS.getMsg(), appUserDTO);
      LOG.debug("[RES] getAppUserDetail, response:[{}]", responseDTO);
      return responseDTO;
    } catch (ServiceException e) {
      LOG.error("Failed to get app user detail", e);
      return ResponseUtil.constructGenericResponse(e);
    } catch (Exception e) {
      LOG.error("Failed to get app user detail", e);
      return ResponseUtil.constructGenericResponse(
          Error.GET_APP_USER_DETAIL_CTL_EXCEPTION, e.getMessage());
    }
  }
}
